import $ from 'jquery'

// const  t = localStorage.currentUser?JSON.parse(localStorage.currentUser).token:"";
export default function(url, data, f, e) {
  const token = localStorage.currentUser ? JSON.parse(localStorage.currentUser).token : ''
  console.log('----data---->' + JSON.stringify(data) + '----token----->' + token + '----!')
  $.ajax({
    type: 'POST',
    url: 'http://localhost:8080' + url,
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      'Authorization': 'SANGE ' + token
    },
    data: JSON.stringify(data),
    success: f,
    fail: function(data) {
      console.log('fail=' + data.status)
    },
    error: e || function(data) {
      console.log('error=' + data.status)
      if (data.status === 403) {
        console.log(data.status)
      }
    }
  })
}
